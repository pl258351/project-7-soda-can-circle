#import the opencv library as cv2 reference,import the numpy library which is a Python library used for working with arrays.
#Only these two libraries are needed.
import numpy as np
import cv2 as cv2


#Capture/read the video file in mp4 format in the correct path.
#MOV format can not be identified here.
#Similar to the things in robotLibrary
img=cv2.VideoCapture('test.mp4')


while True:
    ret,frame = img.read(0)
    if not ret: break

    grayFrame=cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)

    # Blur using 3 * 3 kernel.
    gray_blur = cv2.blur(grayFrame, (3,3))

    output=frame.copy()
    # Apply Hough transform on the blurred image.
    # As long as the min and max is appropriate it should work fine (like min=100 and max=300, but the inital must be set up to a very big number.
    #Otherwise it just detects tons of circles at the same time.
    
    circle=cv2.HoughCircles(gray_blur,cv2.HOUGH_GRADIENT,1.2,5000, param1=100, param2=50, minRadius=75, maxRadius=400)

    # Draw circles that are detected.
    if circle is not None:
        # Convert the circle parameters a, b and r to integers.
        circle= np.uint16(np.around(circle))
        # chosen=None
        for pt in circle[0,:]:
        # Draw the circumference of the general circle.
        # Draw a small circle (of radius 1) to show the center of the Soda Can.
            x,y,r = pt[0],pt[1],pt[2]
            cv2.circle(output, (x,  y),r, (0, 255, 0) , 2)
            cv2.circle(output, (x, y), 1, (0, 255, 0) , 3)

            cv2.imshow("frame",output)
    if cv2.waitKey(1) & 0xFF == ord('q'):
       break
cv2.destroyAllWindows()
